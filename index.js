console.log("Hello Leonardo");

// function printInfo(){
// 	let nickName = prompt ("Enter your nickname");
// 	console.log("Hi, " + nickName);
// }

// printInfo();


function printName(firstName){  /*name is parameter*/
	console.log("My name is " + firstName);
}

printName("Juana"); /*juana is argument*/
printName("Mine"); /*we can reuse it as many as we can*/
printName("Leonardo");

// now we can reuse function with different output
// with the help of [parameters and arguments]

/*Parameters*/
	// Name is called a parameters that acts as named variable that exists only inside the function.
	// It is used to store information that is provided to a function when it is called invoke.

// Argument
/*Juana and Mine and Leonardo the data is called argument that was provided of the  function */
// values passed when invoking a function are called arguments
// these arguments are then stored as the parameter within the function

let sampleVariable = "Inday";

printName(sampleVariable);
// Variables can be also be passed as an argument

// ==============================

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + "divided by 8 is "+ remainder);
	let isDivisilityBy8 = remainder === 0; /* will store a value true/false*/
	console.log("Is "+ num + " divisible by 8?");
	console.log(isDivisilityBy8)

}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [SECTION] Function as argument
	// function parameters can also accept functions as arguments
	// some complex functions uses other functions to performe more complicated results


	function argumentFunction(){
		console.log("This function was passed as an argument before the message was prinetd");
	}

	function invokeFunction(argumentFuncttion){
		argumentFunction();
	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

	// =========================

	// [SECTION] Using Multiple Parameters

	function createFullName(firstName, middleName, lastName){
		console.log("My full name is "+ firstName + " " + middleName + " " + lastName);
	}

	createFullName("Leonardo", "Alinsunurin", "Malachico")

	// using variable as an argument

	let firstName = "John";
	let middleName = "Doe"
	let lastName = "Smith"

	createFullName(firstName, middleName, lastName);

	function getDifferenceOf8Minus4(numA, numB){
		console.log("Difference: " + (numA - numB));
	}

	getDifferenceOf8Minus4(8,4); /*no baliktad result to logical error*/

	// [SECTION] RETURN STATEMENT

	function returnFullName(firstName, middleName, lastName){

		// return firstName + " " + middleName + " " + lastName;

		// this line of code will not be printed 
		console.log("This is printed inside the a function")

		let fullName = firstName + " " + middleName + " " + lastName;
		return fullName;
	}

	let completeName = returnFullName("Paul", "Smith", "Jordan");
	console.log(completeName);

	console.log("I am "+ completeName);


	function printPlayerInfo(userName, level, job){
		console.log("Username: " + userName);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("Magpagmahal", "senior", "Prorammer");
	console.log(user1);


